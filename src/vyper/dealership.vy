#This can be generalized (use "name" instead of "makeAndModel", but
#for now we're just using Vehicles)
struct Vehicle:
    id: int128
    makeAndModel: string[100]
    cost: uint256
    sold: bool
    
#Stock: keeps track of Vehicles. Workers: keeps track of workers
#Recipients: keeps track of purchasers
stock: public(map(int128, Vehicle))
owner: public(address)
recipients: public(map(int128, address))
nextVehicleId: public(int128)
nextWorkerId: public(int128)

MAX_VEHICLES: constant(int128) = 20 #We're a small used lot, nothing crazy
maxVehicles: public(int128)
auth: bool
MAX_WORKERS: constant(int128) = 20
workers: address[MAX_WORKERS]

# Setup global variables
@public
def __init__():
    self.owner = msg.sender
    self.nextWorkerId = 0 

# Add Vehicles
@public
@payable
def addVehicle(_makeAndModel: string[100], _cost: uint256):
    self.auth = False
    if msg.sender == self.owner:
        self.auth = True
    else:
        for i in range(MAX_WORKERS):
            if msg.sender == self.workers[i]:
                self.auth = True
    assert self.auth

    nvi: int128 = self.nextVehicleId
    self.stock[nvi] = Vehicle({
        id: nvi,
        makeAndModel: _makeAndModel,
        cost: _cost,
        sold: False
    })
    self.nextVehicleId += 1

#Allows user to buy a vehicle with a given ID
@public
@payable
def buyVehicleById(_id: int128):
    assert _id >= 0
    assert _id < self.nextVehicleId
    self.recipients[_id] = msg.sender
    self.stock[_id].sold = True  

#Authorizes a person to act on behalf of the company
@public 
@payable
def addWorker(_worker: address):
    assert msg.sender == self.owner
    self.workers[self.nextWorkerId] = _worker
    self.nextWorkerId = self.nextWorkerId + 1
   
#From an external API, can confirm that a given user is the buyer of a 
#given car
@public
def confirmBuyer(_buyer: address, _id: int128) -> bool:
    assert self.recipients[_id] == _buyer
    return True
    
#Renders vehicle unpurchaseable, effectively deleting it
#Readding the same vehicle is not possible and must be done as a 
#"new" vehicle
@public
def deleteVehicle(_id: int128) -> int128:
    assert msg.sender == self.owner
    self.stock[_id].id = -1
    self.stock[_id].sold = True #No way to delete this, so just make it unbuyable
    return 0

#Closes up shop
@public
def cashOut():
    assert msg.sender == self.owner
    send(self.owner, self.balance)

