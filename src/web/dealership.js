var eth = new Eth(new Eth.HttpProvider('https://ropsten.infura.io'));
var el = function(id){ return document.querySelector(id); };

if (typeof window.web3 !== 'undefined' && typeof window.web3.currentProvider !== 'undefined') {
  eth.setProvider(window.web3.currentProvider);
} else {
  eth.setProvider(provider); // set to TestRPC if not available
}

var DealershipABI = [{"outputs": [], "inputs": [], "constant": false, "payable": false, "type": "constructor"}, {"name": "addVehicle", "outputs": [], "inputs": [{"type": "string", "name": "_makeAndModel"}, {"type": "uint256", "name": "_cost"}], "constant": false, "payable": true, "type": "function", "gas": 1031889}, {"name": "buyVehicleById", "outputs": [], "inputs": [{"type": "int128", "name": "_id"}], "constant": false, "payable": true, "type": "function", "gas": 71056}, {"name": "addWorker", "outputs": [], "inputs": [{"type": "address", "name": "_worker"}], "constant": false, "payable": true, "type": "function", "gas": 71277}, {"name": "confirmBuyer", "outputs": [{"type": "bool", "name": "out"}], "inputs": [{"type": "address", "name": "_buyer"}, {"type": "int128", "name": "_id"}], "constant": false, "payable": false, "type": "function", "gas": 982}, {"name": "deleteVehicle", "outputs": [{"type": "int128", "name": "out"}], "inputs": [{"type": "int128", "name": "_id"}], "constant": false, "payable": false, "type": "function", "gas": 71246}, {"name": "cashOut", "outputs": [], "inputs": [], "constant": false, "payable": false, "type": "function", "gas": 36154}, {"name": "stock__id", "outputs": [{"type": "int128", "name": "out"}], "inputs": [{"type": "int128", "name": "arg0"}], "constant": true, "payable": false, "type": "function", "gas": 1008}, {"name": "stock__makeAndModel", "outputs": [{"type": "string", "name": "out"}], "inputs": [{"type": "int128", "name": "arg0"}], "constant": true, "payable": false, "type": "function", "gas": 6400}, {"name": "stock__cost", "outputs": [{"type": "uint256", "name": "out"}], "inputs": [{"type": "int128", "name": "arg0"}], "constant": true, "payable": false, "type": "function", "gas": 1074}, {"name": "stock__sold", "outputs": [{"type": "bool", "name": "out"}], "inputs": [{"type": "int128", "name": "arg0"}], "constant": true, "payable": false, "type": "function", "gas": 1104}, {"name": "owner", "outputs": [{"type": "address", "name": "out"}], "inputs": [], "constant": true, "payable": false, "type": "function", "gas": 851}, {"name": "recipients", "outputs": [{"type": "address", "name": "out"}], "inputs": [{"type": "int128", "name": "arg0"}], "constant": true, "payable": false, "type": "function", "gas": 1086}, {"name": "nextVehicleId", "outputs": [{"type": "int128", "name": "out"}], "inputs": [], "constant": true, "payable": false, "type": "function", "gas": 911}, {"name": "nextWorkerId", "outputs": [{"type": "int128", "name": "out"}], "inputs": [], "constant": true, "payable": false, "type": "function", "gas": 941}, {"name": "maxVehicles", "outputs": [{"type": "int128", "name": "out"}], "inputs": [], "constant": true, "payable": false, "type": "function", "gas": 971}];

var Dealership = eth.contract(DealershipABI).at('0x511B44d52E8520F62E01c74216491F9C7d2E0e9c');

console.log(Dealership);

async function getMakeAndModel(id){
  await Dealership.stock__makeAndModel(id).then((result) => {
    return result
  });
}

async function getMakeAndModelList(listID, listSold){
  for(i = 0; i < 20; i++){
    await Dealership.stock__sold(i).then(async (soldResult) => {
      if(soldResult.out === false || listSold === true){
        await Dealership.stock__makeAndModel(i).then(async (modleResult) => {
          if(modleResult.out != ""){
            await Dealership.stock__cost(i).then((costResult) => {
              var node = document.createElement("LI");
              var textnode = document.createTextNode(modleResult.out + ": $" + costResult.out);
              node.appendChild(textnode);
              document.getElementById(listID).appendChild(node);
            });
          }
        });
      }
    });
  }
}

async function addMakeAndModel(make, cost, fromAddress){
  await Dealership.addVehicle(make, cost, {from: fromAddress}).then(function(result){
    document.getElementById("resultResponse").innerHTML = "result: " + result;
  });
}

async function addWorker(workerAddress, fromAddress){
  await Dealership.addWorker(workerAddress, {from: fromAddress}).then(function(result){
    document.getElementById("resultResponse").innerHTML = "result: " + result;
  });
}

async function deleteVehicle(make, fromAddress){
  await Dealership.deleteVehicle(make, {from: fromAddress}).then(function(result){
    document.getElementById("resultResponse").innerHTML = "result: " + result;
  });
}

async function buyVehicle(make, fromAddress){
  await Dealership.buyVehicleById(make, {from: fromAddress}).then(function(result){
    document.getElementById("resultResponse").innerHTML = "result: " + result;
  });
}

async function cashOut(fromAddress){
  await Dealership.cashOut({from: fromAddress}).then(function(result){
    document.getElementById("resultResponse").innerHTML = "result: " + result;
  });
}

async function getMakeAndModelListOptions(listSold){
  for(i = 0; i < 20; i++){
    await Dealership.stock__sold(i).then(async (soldResult) => {
      if(soldResult.out === false || listSold === true){
        await Dealership.stock__makeAndModel(i).then(async (modleResult) => {
          if(modleResult.out != ""){
            await Dealership.stock__cost(i).then((costResult) => {
              make.innerHTML += '<option value="' + i + '">' + modleResult.out + '</option>';
            });
          }
        });
      }
    });
  }
}
